# Dojo de CI/CD com Gitlab e AWS

Este projeto contém o estado inicial do dojo de CI/CD do treinamento **Certified Scrum Developer** e **Testes Automatizados** da Knowledge21.

O estado inicial está na branch _master_, enquanto a resolução padrão está na branch _solucao_

## API Calculadora Comissão

Para rodar a API é preciso primeiro ter o Pyton 3.4 ou superior instalado e rodar o comando `pip install -r requirements.txt`.

O comando para iniciar a API é: `gunicorn -b 0.0.0.0:8000 wsgi:app`, estará disponível em `http://localhost:8000/comissao/<float>`

**IMPORTANTE** O valor float tem que ter casas decimais ou a APi dará erro 404.

## Requisitos

- **Python 3** - O projeto foi criado no Python 3.8.0, mas funciona nas versões 3.4 em diante;
- **Ter uma conta na AWS e o awscli instalado** - É fundamental ter uma conta da AWS criada e configurada. *Se estiver no período inicial, os recursos criados aqui entram no free tier*
- **Usuário administrador AWS** -  É preciso ter um usuário administrador na conta AWS com acesso programático (awscli) para criar a infraestrutura e fazer o deploy;
- **Repositório no Gitlab** - Um repositório vazio para receber o código que iremos enviar;  

## Preparação

A preparação para o Dojo deve ser realizada previamente e consiste dos seguintes passos:  

1. Criar o usuário administrador com acesso programático da seguinte forma:
    1. Criar um usuário administrador com acesso programatico e salvar as credenciais dele para uso posterior;
    2. Usando o comando `aws configure`, configurar o cliente da AWS, usando as credenciais do usuário criado no passo anterior. **IMPORTANTE** Preste atenção na região padrão. Será usado mais tarde;
    3. Criar a infraestrutura usando o comando `aws cloudformation create-stack --stack-name CSD --template-body file://infra.yaml --capabilities CAPABILITY_IAM` na raiz do projeto. A infraestrutura será criada na região padrão.

## Detalhes para a execução do Dojo

- O Gitlab permite criar variáveis de ambiente no pipeline. Para esse dojo usamos 4:
    1. AWS_ACCESS_KEY_ID - Corresponde à credencial do usuário da AWS;
    2. AWS_SECRET_ACCESS_KEY - Idem;
    3. AWS_DEFAULT_REGION - Corresponde à região usada na configuração da AWS.
    4. REPOSITORY_URI - É usada para armazenar endereço do repositório de containers. É acessível pelo comando `aws ecr describe-repositories`, na chave "repositoryUri"; 

O repositório do dojo tem 2 branches: `master`, que contém o etado inicial e `solucao` que contém o estado final.

